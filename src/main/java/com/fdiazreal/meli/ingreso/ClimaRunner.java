package com.fdiazreal.meli.ingreso;

import com.fdiazreal.meli.ingreso.config.AppConfig;
import com.fdiazreal.meli.ingreso.model.SolarSystem;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class ClimaRunner {


	private static final int DAYS_IN_10_YEARS = 10 * 365;

	public static void main(final String[] args) {

		SolarSystem solarSystem = AppConfig.createUniverse();

		solarSystem.runFor(DAYS_IN_10_YEARS);
	}
}
