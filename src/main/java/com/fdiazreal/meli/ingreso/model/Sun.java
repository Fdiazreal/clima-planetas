package com.fdiazreal.meli.ingreso.model;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class Sun implements Positionable {

	private static PositionInSystem position = new PositionInSystem(0, 0);

	@Override
	public PositionInSystem getCurrentPosition(){
		return position;
	}
}
