package com.fdiazreal.meli.ingreso.model;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public interface Positionable {

	/**
	 * @return current position in the Solar System
	 */
	public PositionInSystem getCurrentPosition();

}
