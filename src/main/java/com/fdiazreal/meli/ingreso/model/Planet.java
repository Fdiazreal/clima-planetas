package com.fdiazreal.meli.ingreso.model;

/**
 * Models a Planet of the Solar System
 *
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class Planet implements Positionable {

	private final int distanceToSun;
	private final int degreesPerDay;
	private int currentDegrees;

	/**
	 * @param distanceToSun linear distance to the Sun
	 * @param degreesPerDay degrees that the planet moves per day
	 */
	public Planet(int distanceToSun, int degreesPerDay) {
		this.distanceToSun = distanceToSun;
		this.degreesPerDay = degreesPerDay;
	}

	/**
	 * Performs necessary operations that a planet needs every day
	 */
	public void newDay(){
		currentDegrees = (currentDegrees + degreesPerDay) % 360;

		if(currentDegrees < 0){
			currentDegrees = currentDegrees + 360;
		}
	}

	@Override
	public PositionInSystem getCurrentPosition(){
		return new PositionInSystem(distanceToSun, currentDegrees);
	}

	public int getCurrentDegrees(){
		return currentDegrees;
	}

}
