package com.fdiazreal.meli.ingreso.model;

import com.fdiazreal.meli.ingreso.climate.MeteorologicalSystem;

import java.util.List;

/**
 * Runs an Era, by changing the day and notifying the {@link MeteorologicalSystem}
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class SolarSystem {

	private final Sun sun;
	private final List<Planet> planets;
	private MeteorologicalSystem meteorologicalSystem;

	public SolarSystem(Sun sun, List<Planet> planets, MeteorologicalSystem meteorologicalSystem) {
		this.sun = sun;
		this.planets = planets;
		this.meteorologicalSystem = meteorologicalSystem;
	}

	/**
	 * Starts a new Era of the {@link SolarSystem}
	 * @param daysToRun Number of days that the Era will have
	 */
	public void runFor(int daysToRun) {

		for(int day = 0; day < daysToRun; day++){
			System.out.println("Day <" + day + 1 + ">");
			planets.forEach(planet -> planet.newDay());
			System.out.println(	"Planet 1 <" + planets.get(0).getCurrentDegrees() + "> "
								+ "Planet 2 <" + planets.get(1).getCurrentDegrees() + "> "
								+ "Planet 3 <" + planets.get(2).getCurrentDegrees() + "> ");

			meteorologicalSystem.determineWeather(day);
			System.out.println("#-------------------------------------------------------#");
		}

		meteorologicalSystem.endOfEra();

	}
}
