package com.fdiazreal.meli.ingreso.model;

/**
 * Represents a position in the Solar System, allowing to do some calculations with other positions.
 * Since there are no requirements of precision, everything is rounded.
 *
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class PositionInSystem {

	private final int x;
	private final int y;

	public PositionInSystem(int distanceToSun, int degrees) {
		double radians = Math.toRadians(degrees);
		this.x = calculateCoord(distanceToSun, Math.cos(radians));
		this.y = calculateCoord(distanceToSun, Math.sin(radians));
	}

	private int calculateCoord(int distance, double angle){
		return (int) Math.round((distance * angle));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	/**
	 * Indicates if this position is aligned with other 2 positions. Hence, all 3 positions are part of a line.
	 *
	 * @param position1
	 * @param position2
	 * @return True if it is aligned, false if not.
	 */
	public boolean isAlignedWith(PositionInSystem position1, PositionInSystem position2) {
		long symmetricA = (position2.getX() - position1.getX()) * (this.getY() - position1.getY());
		long symmetricB = (position2.getY() - position1.getY()) * (this.getX() - position1.getX());

		return symmetricA == symmetricB;
	}

	/**
	 * Calculates the distance to another position.
	 *
	 * @param anotherPosition
	 * @return The distance
	 */
	public long distanceTo(PositionInSystem anotherPosition){
		double firstTerm = Math.pow(anotherPosition.getX() - this.getX(), 2);
		double secondTerm = Math.pow(anotherPosition.getY() - this.getY(), 2);

		return Math.round(Math.sqrt(firstTerm + secondTerm));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PositionInSystem that = (PositionInSystem) o;

		if (x != that.x) return false;
		if (y != that.y) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = x;
		result = 31 * result + y;
		return result;
	}
}
