package com.fdiazreal.meli.ingreso.config;

import com.fdiazreal.meli.ingreso.climate.MeteorologicalSystem;
import com.fdiazreal.meli.ingreso.climate.WeatherSubscriber;
import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;
import com.fdiazreal.meli.ingreso.model.SolarSystem;
import com.fdiazreal.meli.ingreso.stats.DailyStats;
import com.fdiazreal.meli.ingreso.stats.MaxPrecipitationStat;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the creation of the App's model.
 *
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class AppConfig {

	public static SolarSystem createUniverse(){

		Sun sun = createSun();
		List<Planet> planets = createPlanets();

		MeteorologicalSystem meteorologicalSystem = createMeteorologicSystem(sun, planets);

		meteorologicalSystem.registerClimateSubscriber(createDailyStats());
		meteorologicalSystem.registerClimateSubscriber(createMaxPrecipitationStat());

		SolarSystem solarSystem = new SolarSystem(sun, planets, meteorologicalSystem);

		return solarSystem;
	}


	private static Sun createSun(){
		return new Sun();
	}

	private static List<Planet> createPlanets(){

		Planet ferengi = new Planet(500, -1);
		Planet betasoide = new Planet(2000, -3);
		Planet vulcano = new Planet(1000, 5);

		List<Planet> planets = new ArrayList<Planet>();

		planets.add(ferengi);
		planets.add(betasoide);
		planets.add(vulcano);

		return planets;
	}

	private static MeteorologicalSystem createMeteorologicSystem(final Sun sun, final List<Planet> planets){
		return new MeteorologicalSystem(sun, planets);
	}

	private static WeatherSubscriber createMaxPrecipitationStat() {
		return new MaxPrecipitationStat();
	}

	private static WeatherSubscriber createDailyStats() {
		return new DailyStats();
	}


}
