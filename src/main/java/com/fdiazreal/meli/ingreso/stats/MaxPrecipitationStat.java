package com.fdiazreal.meli.ingreso.stats;

import com.fdiazreal.meli.ingreso.climate.WeatherSubscriber;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class MaxPrecipitationStat implements WeatherSubscriber {

	private long maxRain;
	private long dayOfMaxRain;

	@Override
	public void onRain(final long numberOfDay, final long amountOfRain) {
		if(amountOfRain > maxRain){
			maxRain = amountOfRain;
			dayOfMaxRain = numberOfDay;
		}
	}

	@Override
	public void onDrought(final long numberOfDay) {
		// Nothing
	}

	@Override
	public void onOptimalConditions(final long numberOfDay) {
		// Nothing
	}

	@Override
	public void onUnpredictableWeather(long day) {
		// Nothing
	}

	@Override
	public void endOfPredictions() {
		System.out.println("The day <" + dayOfMaxRain + "> was the day with maximum precipitations");
	}
}
