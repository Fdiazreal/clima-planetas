package com.fdiazreal.meli.ingreso.stats;

import com.fdiazreal.meli.ingreso.climate.WeatherSubscriber;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class DailyStats implements WeatherSubscriber {

	private long daysWithRain;
	private long daysWithDroughts;
	private long daysWithOptimalConditions;
	private long daysWithUnpredictableWeather;

	@Override
	public void onRain(long numberOfDay, long amountOfRain) {
		System.out.println("Raining");
		daysWithRain++;
	}

	@Override
	public void onDrought(long numberOfDay) {
		System.out.println("Drought");
		daysWithDroughts++;
	}

	@Override
	public void onOptimalConditions(long numberOfDay) {
		System.out.println("Optimal Conditions");
		daysWithOptimalConditions++;
	}

	@Override
	public void onUnpredictableWeather(long day) {
		System.out.println("Cannot predict weather");
		daysWithUnpredictableWeather++;
	}

	@Override
	public void endOfPredictions() {
		System.out.println();
		System.out.println("#--- Statistics by the end of times --------------------#");
		System.out.println("There were");
		System.out.println("-> <" + daysWithRain + "> days with rain");
		System.out.println("-> <" + daysWithDroughts + "> days with droughts");
		System.out.println("-> <" + daysWithOptimalConditions + "> days with optimal conditions");
		System.out.println("-> <" + daysWithUnpredictableWeather + "> days with unpredictable weather");
		System.out.println();
	}
}
