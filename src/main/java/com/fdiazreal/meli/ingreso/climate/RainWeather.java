package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.List;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class RainWeather extends Weather {

	public static final int COMPLETE_TURN = 360;

	public RainWeather(Weather nextWeather) {
		super(nextWeather);
	}

	@Override
	public boolean areConditionsMet(Sun sun, List<Planet> planets) {

		Planet planet1 = planets.get(0);
		Planet planet2 = planets.get(1);
		Planet planet3 = planets.get(2);

		int totalDegrees = 	getAngleBetween(planet1, planet2)
							+ getAngleBetween(planet2, planet3)
							+ getAngleBetween(planet1, planet3);

		return totalDegrees == COMPLETE_TURN;
	}

	private int getAngleBetween(Planet first, Planet second) {
		int degrees1 = first.getCurrentDegrees();
		int degrees2 = second.getCurrentDegrees();
		int angle = Math.abs(degrees1 - degrees2);

		if(angle > 180){
			angle = COMPLETE_TURN - angle;
		}

		return angle;
	}

	@Override
	protected void notifyCallback(MeteorologicalSystem callback, long day) {
		callback.onRain(day);
	}
}
