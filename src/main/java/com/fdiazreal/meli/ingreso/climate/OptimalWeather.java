package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.PositionInSystem;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.List;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class OptimalWeather extends Weather {

	public OptimalWeather(Weather nextWeather) {
		super(nextWeather);
	}

	@Override
	public boolean areConditionsMet(Sun sun, List<Planet> planets) {

		PositionInSystem positionPlanet1 = planets.get(0).getCurrentPosition();
		PositionInSystem positionPlanet2 = planets.get(1).getCurrentPosition();
		PositionInSystem positionPlanet3 = planets.get(2).getCurrentPosition();

		PositionInSystem sunPosition = sun.getCurrentPosition();

		boolean planetsAligned = positionPlanet1.isAlignedWith(positionPlanet2, positionPlanet3);
		boolean planetsAlignedWithSun = sunPosition.isAlignedWith(positionPlanet1, positionPlanet2);

		return planetsAligned && !planetsAlignedWithSun;
	}

	@Override
	protected void notifyCallback(MeteorologicalSystem callback, long day) {
		callback.onOptimalConditions(day);
	}
}
