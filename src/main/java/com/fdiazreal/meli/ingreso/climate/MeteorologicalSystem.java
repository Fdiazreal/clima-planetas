package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.PositionInSystem;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.ArrayList;
import java.util.List;

/**
 * Finds the {@link Weather} for the given {@link com.fdiazreal.meli.ingreso.model.SolarSystem} state and notifies
 * the subscribers about it.
 *
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class MeteorologicalSystem {

	private final Sun sun;
	private final List<Planet> planets;

	private final Weather weather;

	private List<WeatherSubscriber> weatherSubscribers = new ArrayList<>();

	public MeteorologicalSystem(final Sun sun, final List<Planet> planets){
		this.sun = sun;
		this.planets = planets;

		Weather nullWeather = new NullWeather();
		Weather rain = new RainWeather(nullWeather);
		Weather optimalConditions = new OptimalWeather(rain);
		weather = new DroughtWeather(optimalConditions);
	}

	/**
	 * Subscribes a member to get notifications of the {@link Weather}
	 * @param subscriber
	 */
	public void registerClimateSubscriber(WeatherSubscriber subscriber) {
		weatherSubscribers.add(subscriber);
	}

	/**
	 * Finds the {@link Weather} of the day
	 * @param day Number of day in the {@link com.fdiazreal.meli.ingreso.model.SolarSystem}
	 */
	public void determineWeather(final long day) {

		weather.checkCompliance(sun, planets, this, day);

	}

	/**
	 * Calculates the final statistics at the end of an Era.
	 */
	public void endOfEra() {
		weatherSubscribers.forEach(subscriber -> subscriber.endOfPredictions());
	}

	protected void onRain(long day) {
		long amountOfRain = calculatePerimeter();
		weatherSubscribers.forEach(subscriber -> subscriber.onRain(day, amountOfRain));
	}

	private long calculatePerimeter() {
		PositionInSystem planet1Position = planets.get(0).getCurrentPosition();
		PositionInSystem planet2Position = planets.get(1).getCurrentPosition();
		PositionInSystem planet3Position = planets.get(2).getCurrentPosition();

		return planet1Position.distanceTo(planet2Position)
				+ planet2Position.distanceTo(planet3Position)
				+ planet3Position.distanceTo(planet1Position);
	}

	protected void onDrought(long day) {
		weatherSubscribers.forEach(subscriber -> subscriber.onDrought(day));
	}

	protected void onOptimalConditions(long day) {
		weatherSubscribers.forEach(subscriber -> subscriber.onOptimalConditions(day));
	}

	protected void weatherUnpredictable(long day) {
		weatherSubscribers.forEach(subscriber -> subscriber.onUnpredictableWeather(day));
	}
}
