package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.PositionInSystem;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.List;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class DroughtWeather extends Weather {

	public DroughtWeather(Weather nextWeather) {
		super(nextWeather);
	}

	@Override
	protected boolean areConditionsMet(Sun sun, List<Planet> planets) {

		PositionInSystem positionPlanet1 = planets.get(0).getCurrentPosition();
		PositionInSystem positionPlanet2 = planets.get(1).getCurrentPosition();
		PositionInSystem positionPlanet3 = planets.get(2).getCurrentPosition();

		PositionInSystem sunPosition = sun.getCurrentPosition();

		return sunPosition.isAlignedWith(positionPlanet1, positionPlanet2)
				&& sunPosition.isAlignedWith(positionPlanet2, positionPlanet3);
	}

	@Override
	protected void notifyCallback(MeteorologicalSystem callback, long day) {
		callback.onDrought(day);
	}
}
