package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.List;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class NullWeather extends Weather {

	public NullWeather() {
		super(null);
	}

	@Override
	protected boolean areConditionsMet(Sun sun, List<Planet> planets) {
		return true;
	}

	@Override
	protected void notifyCallback(MeteorologicalSystem callback, long day) {
		callback.weatherUnpredictable(day);
	}
}
