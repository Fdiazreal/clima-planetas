package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.SolarSystem;

/**
 * A {@link WeatherSubscriber} will receive notifications when the {@link SolarSystem} {@link Weather} is determined
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public interface WeatherSubscriber {

	/**
	 * Called when it is raining in the {@link SolarSystem}
	 * @param numberOfDay
	 * @param amountOfRain
	 */
	public void onRain(final long numberOfDay, final long amountOfRain);

	/**
	 * Called when the {@link SolarSystem} is experimenting a drought
	 * @param numberOfDay
	 */
	public void onDrought(final long numberOfDay);

	/**
	 * Called when there are Optimal Conditions of {@link Weather}
	 * @param numberOfDay
	 */
	public void onOptimalConditions(final long numberOfDay);

	/**
	 * Called when the {@link Weather} cannot be determined
	 * @param day
	 */
	public void onUnpredictableWeather(long day);

	/**
	 * Do necessary calculations after the last prediction
	 */
	public void endOfPredictions();
}
