package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;

import java.util.List;

/**
 * Checks if the {@link com.fdiazreal.meli.ingreso.model.SolarSystem} is compliant with the given {@link Weather}
 * Subclasses should implement the necessary logic for each weather and call the appropiate method of the callback.
 *
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public abstract class Weather {

	private Weather nextWeather;

	public Weather(final Weather nextWeather){
		this.nextWeather = nextWeather;
	}

	/**
	 * Verify if the conditions are met for the given {@link Weather}
	 * @param sun
	 * @param planets
	 * @return True if the Solar System has that {@link Weather}.
	 */
	protected abstract boolean areConditionsMet(Sun sun, List<Planet> planets);

	/**
	 * Calls the {@link MeteorologicalSystem} back to inform the current weather of the {@link com.fdiazreal.meli.ingreso.model.SolarSystem}
	 * @param callback
	 * @param day
	 */
	protected abstract void notifyCallback(MeteorologicalSystem callback, long day);

	public void checkCompliance(Sun sun, List<Planet> planets, MeteorologicalSystem callback, long day){
		if (areConditionsMet(sun, planets)){
			notifyCallback(callback, day);
		} else {
			nextWeather.checkCompliance(sun, planets, callback, day);
		}
	}

}
