import org.junit.Test;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class ATest {
	@Test
	public void testa(){
		double rad90 = Math.toRadians(90);
		System.out.println(rad90);
		System.out.println(rad90 == Math.PI/4);

		double cos90 = Math.cos(rad90);
		System.out.println(cos90);
		System.out.println(cos90 == 0d);
		System.out.println(Math.sin(Math.toRadians(90d)));
		System.out.println(Math.cos(Math.toRadians(0d)));
		System.out.println(Math.sin(Math.toRadians(0d)));
		System.out.println(Math.cos(Math.toRadians(180d)));
		double sin180 = Math.sin(Math.toRadians(180d));
		System.out.println(sin180);
		System.out.println(sin180 == 0d);
		System.out.println(Math.cos(Math.toRadians(270d)));
		System.out.println(Math.sin(Math.toRadians(270d)));
	}
}
