package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.PositionInSystem;
import com.fdiazreal.meli.ingreso.model.Sun;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class DroughtWeatherTest {

	public static final int DAY = 1;

	private DroughtWeather droughtWeather;

	@Mock private Sun sun;
	@Mock private Planet planet1;
	@Mock private Planet planet2;
	@Mock private Planet planet3;
	@Mock private Weather nullWeather;
	@Mock private PositionInSystem position1;
	@Mock private PositionInSystem position2;
	@Mock private PositionInSystem position3;
	@Mock private PositionInSystem sunPosition;
	@Mock private MeteorologicalSystem callback;

	List<Planet> planets = new ArrayList<>();

	@Before
	public void setUp(){
		initMocks(this);

		droughtWeather = new DroughtWeather(nullWeather);

		when(sun.getCurrentPosition()).thenReturn(sunPosition);
		when(planet1.getCurrentPosition()).thenReturn(position1);
		when(planet2.getCurrentPosition()).thenReturn(position2);
		when(planet3.getCurrentPosition()).thenReturn(position3);

		planets.add(planet1);
		planets.add(planet2);
		planets.add(planet3);
	}

	@Test
	public void testWeatherRecognizesEverythingAligned(){
	// Given
		when(sunPosition.isAlignedWith(position1, position2)).thenReturn(true);
		when(sunPosition.isAlignedWith(position2, position3)).thenReturn(true);

	// When
		boolean isDrought = droughtWeather.areConditionsMet(sun, planets);

	// Then
		assertTrue(isDrought);
	}

	@Test
	public void testWeatherRecognizesPlanetsAreNotAligned(){
	// Given
		when(sunPosition.isAlignedWith(position1, position2)).thenReturn(true);
		when(sunPosition.isAlignedWith(position2, position3)).thenReturn(false);

	// When
		boolean isDrought = droughtWeather.areConditionsMet(sun, planets);

	// Then
		assertFalse(isDrought);
	}

	@Test
	public void testOptimalWeatherOnMetSystem(){

	// When
		droughtWeather.notifyCallback(callback, DAY);

	// Then
		verify(callback).onDrought(DAY);
		verifyNoMoreInteractions(callback);
	}

}