package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

public class NullWeatherTest {

	public static final int DAY = 1;
	private NullWeather nullWeather = new NullWeather();

	@Mock private MeteorologicalSystem callback;

	private Sun sun = new Sun();
	private List<Planet> planets = new ArrayList<>();

	@Before
	public void setUp(){
	    initMocks(this);
	}

	@Test
	public void testConditionsAlwaysMet(){
	// When
		boolean acceptsWeather = nullWeather.areConditionsMet(sun, planets);

	// Then
		assertTrue(acceptsWeather);
	}

	@Test
	public void testUnpredictableWeatherCalledOnMetSystem(){
	// When
		nullWeather.notifyCallback(callback, DAY);

	// Then
	    verify(callback).weatherUnpredictable(DAY);
		verifyNoMoreInteractions(callback);
	}

}