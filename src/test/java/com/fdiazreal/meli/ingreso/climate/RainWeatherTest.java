package com.fdiazreal.meli.ingreso.climate;

import com.fdiazreal.meli.ingreso.model.Planet;
import com.fdiazreal.meli.ingreso.model.Sun;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RainWeatherTest extends TestCase {

	public static final int DAY = 1;
	RainWeather rain;

	@Mock
	MeteorologicalSystem metSystem;
	@Mock Weather nullWeather;
	@Mock Planet planet1;
	@Mock Planet planet2;
	@Mock Planet planet3;

	List<Planet> planets = new ArrayList<>();
	private Sun sun;

	@Before
	public void setUp(){
	    initMocks(this);

		rain = new RainWeather(nullWeather);

		planets.add(planet1);
		planets.add(planet2);
		planets.add(planet3);
	}

	@Test
	public void testSunIsInsideABasicTriangle(){
	// Given
	    when(planet1.getCurrentDegrees()).thenReturn(180);
	    when(planet2.getCurrentDegrees()).thenReturn(90);
	    when(planet3.getCurrentDegrees()).thenReturn(0);

	// When
		boolean isRaining = rain.areConditionsMet(sun, planets);

	// Then
		assertTrue(isRaining);
	}

	@Test
	public void testSunIsInsideA30_90_210(){
	// Given
	    when(planet1.getCurrentDegrees()).thenReturn(30);
	    when(planet2.getCurrentDegrees()).thenReturn(90);
	    when(planet3.getCurrentDegrees()).thenReturn(220);

	// When
		boolean isRaining = rain.areConditionsMet(sun, planets);

	// Then
		assertTrue(isRaining);
	}

	@Test
	public void testCallsRainOnMetSystem(){
	// When
		rain.notifyCallback(metSystem, DAY);

	// Then
	    verify(metSystem).onRain(DAY);
		verifyNoMoreInteractions(metSystem);
	}

}