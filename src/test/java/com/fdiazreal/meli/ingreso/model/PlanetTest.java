package com.fdiazreal.meli.ingreso.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Federico Diaz Real (f.diazreal@gmail.com)
 */
public class PlanetTest {

    private Planet planet;

    @Test
    public void testPlanetChangesThePositionEveryDay(){
    // Given
        planet = new Planet(500, 10);

    // When
        PositionInSystem position1 = planet.getCurrentPosition();

    // Then
        assertEquals(new PositionInSystem(500, 0), position1);

    // When
        planet.newDay();
        PositionInSystem position2 = planet.getCurrentPosition();

    // Then
        assertEquals(new PositionInSystem(500, 10), position2);

    // When
        planet.newDay();
        PositionInSystem position3 = planet.getCurrentPosition();

    // Then
        assertEquals(new PositionInSystem(500, 20), position3);
    }
    
    @Test
    public void testPlanetCompletesAYear(){
    // Given: 60 degrees per day
        planet = new Planet(500, 60);
        
    // When: the planet moves 6 days -> 360 degrees
        for(int i = 0; i < 6; i++){
            planet.newDay();
        }

        PositionInSystem position = planet.getCurrentPosition();

    // Then
        assertEquals(position, new PositionInSystem(500, 0));
    }
}
