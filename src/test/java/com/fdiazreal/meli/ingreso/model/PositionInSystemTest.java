package com.fdiazreal.meli.ingreso.model;

import junit.framework.TestCase;
import org.junit.Test;

public class PositionInSystemTest extends TestCase {
	
	private long x;
	private long y;

	@Test
	public void testSystemPositionForPositiveX(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 0);

	// When
		x = position.getX();
		y = position.getY();

	// Then
		assertEquals(500, x);
		assertTrue(0 == y);
	}

	@Test
	public void testSystemPositionForNegativeX(){
	// Given
		PositionInSystem position = new PositionInSystem(-500, 0);

	// When
		x = position.getX();
		y = position.getY();

	// Then
		assertEquals(-500, x);
		assertTrue(0 == y);
	}

	@Test
	public void testSystemPositionForPositiveY(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 90);

	// When
		x = position.getX();
		y = position.getY();

	// Then
		assertTrue(0 == x);
		assertEquals(500, y);
	}

	@Test
	public void testSystemPositionForNegativeY(){
	// Given
		PositionInSystem position = new PositionInSystem(500, -90);

	// When
		x = position.getX();
		y = position.getY();

	// Then
		assertTrue(0 == x);
		assertEquals(-500, y);
	}

	@Test
	public void testSystemPositionFor45(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 45);

	// When
		x = position.getX();
		y = position.getY();

	// Then
		assertEquals(x, y);
	}

	@Test
	public void test3PositionsAreAlignedOnX(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 0);
	    PositionInSystem position2 = new PositionInSystem(200, 0);
	    PositionInSystem position3 = new PositionInSystem(300, 0);

	// When
		boolean isAligned = position.isAlignedWith(position2, position3);

	// Then
		assertTrue(isAligned);
	}

	@Test
	public void test3PositionsAreAlignedOnY(){
		// Given
		PositionInSystem position = new PositionInSystem(500, 90);
		PositionInSystem position2 = new PositionInSystem(200, 90);
		PositionInSystem position3 = new PositionInSystem(300, 90);

		// When
		boolean isAligned = position.isAlignedWith(position2, position3);

		// Then
		assertTrue(isAligned);
	}

	@Test
	public void test3PositionsAreAlignedOn45degrees(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 45);
		PositionInSystem position2 = new PositionInSystem(200, 45);
		PositionInSystem position3 = new PositionInSystem(300, 225);

	// When
		boolean isAligned = position.isAlignedWith(position2, position3);

	// Then
		assertTrue(isAligned);
	}



	@Test
	public void test3PositionsAreNotAligned(){
	// Given
		PositionInSystem position = new PositionInSystem(500, 90);
		PositionInSystem position2 = new PositionInSystem(200, -90);
		PositionInSystem position3 = new PositionInSystem(300, 0);

	// When
		boolean isAligned = position.isAlignedWith(position2, position3);

	// Then
		assertFalse(isAligned);
	}

	@Test
	public void testDistanceBetweenTwoPositions(){
	// Given
		PositionInSystem position1 = new PositionInSystem(200, 0);
		PositionInSystem position2 = new PositionInSystem(300, 0);

	// When
		long distance = position1.distanceTo(position2);

	// Then
		assertEquals(100, distance);
	}

	@Test
	public void testDistanceBetweenTwoPositionsWithDifferentAngles(){
	// Given
		PositionInSystem position1 = new PositionInSystem(300, 0);
		PositionInSystem position2 = new PositionInSystem(400, 90);

	// When
		long distance = position1.distanceTo(position2);

	// Then
		assertEquals(500, distance);
	}

}