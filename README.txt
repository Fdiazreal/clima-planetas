#######################################################
##  Clima Planetas                                   ##
#######################################################

#-----------------------------------------------------#
# Entorno necesario                                   #
#-----------------------------------------------------#
JDK 1.8
Maven 3

#-----------------------------------------------------#
# Como ejecutarlo                                     #
#-----------------------------------------------------#
1) Compilar la aplicación corriendo
	> mvn assembly:assembly
2) Ir a /target/clima-planetas-0.0.1-SNAPSHOT-dist
3) Ejecutar 
	> ./run.sh
4) Se genera un archivo log con el resultado